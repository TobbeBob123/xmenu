#!/bin/sh

xmenu <<EOF | sh &
Refresh			xmonad --recompile; xmonad --restart
Lock			light-locker-command -l

Log Off			pkill -u $USER
Reboot			reboot
Shutdown		poweroff
EOF
